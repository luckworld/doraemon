# coding=utf-8

# this class define info about concept stock index info, like wind concept stocks
__author__ = 'a11'

from WindPy import w
import math
import time
import logging
import sys


class StockIndex:
    def __init__(self, code=""):
        self.fifteen_volumes = []
        self.code = code
        self.children_wind_codes = []
        self.children = []
        # self.root_wind_code = "884001.WI"
        pass

    def get_children_codes(self):
        date_str = time.strftime("%Y%m%d", time.localtime(time.time()))
        codes = w.wset("SectorConstituent", date=date_str, windcode=self.code)
        if codes.ErrorCode != 0:
            print codes
            return
        map_info = dict(zip(codes.Fields, codes.Data))
        self.children_wind_codes = map_info['wind_code']
        return

    @staticmethod
    def get_node_sector_constituent(code,
                                    date_str=time.strftime("%Y%m%d", time.localtime(time.time()))
                                    ):
        # w.wset('SectorConstituent','date=20150730;windcode=884001.WI')
        codes = w.wset("SectorConstituent", date=date_str, windcode=code)
        if codes.ErrorCode != 0:
            print codes
            return
        map_info = dict(zip(codes.Fields, codes.Data))
        win_codes = map_info['wind_code']

        return win_codes

