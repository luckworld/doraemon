# coding=utf-8

from WindPy import w

from utils.StockUtil import *
from domain.Stock import *

from sys import *
import sqlite3
import time, datetime

'''
按照开关，环境变量，source文件，常量，变量，函数，主函数/主逻辑的顺序书写脚本
#!/bin/bash
# 1. 开关
set -x # set -o xtrace
# 2. 环境变量
PATH=/home/abc/bin:$PATH
export PATH
# 3. source文件
source some_lib.sh
# 4. 常量
readonly PI=3.14
# 5. 变量
my_var=1
# 6. 函数
# 函数注释，格式参考注释章节
function main() {
}
# 7. 主函数/主逻辑
main "$@"
'''

'''
常量&变量
'''
# #prepare phase: 计算平均成交量等
# 医药行业五只股票http://stock.eastmoney.com/hangye/hy465.html
# codesList = ["600216.SH", "600572.SH", "300086.SH", "002432.SZ", "002107.SZ"]
codesList = ["600216.SH", "600572.SH"]
indexName = "SUM"

stockMap = {}
for code in codesList:
    stockMap[code] = Stock(code)
stockMap[indexName] = Stock(indexName)

useTest = True
startTime = datetime.datetime.strptime("2015-09-01", "%Y-%m-%d")
endTime = datetime.datetime.strptime("2015-09-30", "%Y-%m-%d")  # %H:%M:%S")
if useTest:
    print "testing..."
else:
    print "analogy..."
    startTime = datetime.datetime().now()  # datetime.datetime.now().strftime("%Y-%m-%d 09:00:00")
    endTime = datetime.datetime.strptime(datetime.datetime.now().strftime("%Y-%m-%d 15:00:00"), "%Y-%m-%d %H:%M:%S")

'''
逻辑过程
'''
w.start()

codesVolData = stocks_average_volume(codesList, get_offset_trade_date("2015-10-01", -1), 5)
avgVolVec = np.mean(codesVolData.Data, axis=1)  # 1 columns, 0 rows
codesMap = dict(zip(codesVolData.Codes, avgVolVec))
codesMap[indexName] = sum(avgVolVec)

# 将前五天的平均成交量合并到avg_vol中来
for (k, v) in stockMap.iteritems():
    v.avg_vol = codesMap[k]
print stockMap.items()

# init db op: sqlite3 数据库
# con = sqlite3.connect(":memory:")
con = sqlite3.connect("test_lite3.db")
cur = con.cursor()
res = cur.execute("select count(1) from sqlite_master where type=\"table\" and name=\"market\";")
res = res.fetchall()
if res[0][0] == 0:
    cur.execute("create table market(id integer primary key autoincrement, wind_code VARCHAR(30), time INTEGER, "
                "volume REAL, price REAL);")
else:
    cur.execute("delete from market")  # 清理数据
    cur.execute("update sqlite_sequence set seq=0 where name='market'")  # 自增长id设置位0

# 读取交易数据，判断数据的信号
r = endTime - startTime
for j in range(1, r.days + 2):
    dayStartTime = startTime + datetime.timedelta(days=j)
    dayStartTime = datetime.datetime.strptime(dayStartTime.strftime("%Y-%m-%d 09:30:00"), "%Y-%m-%d %H:%M:%S")
    dayEndTime = datetime.datetime.strptime(dayStartTime.strftime("%Y-%m-%d 15:00:00"), "%Y-%m-%d %H:%M:%S")

    # market_data = w.wsi(codesList, "close,amt", "2015-06-01 9:00:00", "2015-06-01 15:00:00")
    marketData = w.wsi(codesList, "close,amt",
                       dayStartTime.strftime("%Y-%m-%d %H:%M:%S"), dayEndTime.strftime("%Y-%m-%d %H:%M:%S"))
    if marketData.ErrorCode != 0:
        raise ValueError("wsi_data error code != 0")
        exit()
    print marketData
    for i in range(0, len(marketData.Times)):
        try:
            t = (marketData.Data[0][i], marketData.Data[1][i], marketData.Data[2][i], marketData.Data[3][i]);
            con.execute("insert into market( time, wind_code, price, volume) values (?,?,?,?)", t)
        except Exception, ex:
            print Exception, ":", ex
    con.commit()

    # 分钟差
    # select * from market where time > datetime("2015-06-01 14:52:00");
    dayStartTime = datetime.datetime.strptime(dayStartTime.strftime("%Y-%m-%d 09:50:00"), "%Y-%m-%d %H:%M:%S")
    r = dayEndTime - dayStartTime
    for i in range(60, r.seconds + 1, 60):
        currentTime = dayStartTime + datetime.timedelta(seconds=i)
        nextTime = currentTime + datetime.timedelta(seconds=60)
        j = i - 5 * 60
        compareTime = dayStartTime + datetime.timedelta(seconds=j)
        compareNextTime = compareTime + datetime.timedelta(seconds=60)
        for code in codesList:
            t = (currentTime.strftime("%Y-%m-%d %H:%M:%S"), nextTime.strftime("%Y-%m-%d %H:%M:%S"), code)
            r = con.execute("select wind_code, datetime(time), price, volume from market where time >= datetime(?) and "
                            "time < datetime(?) and wind_code=? ", t)
            r = r.fetchone()
            print r

            tcmp = (compareTime.strftime("%Y-%m-%d %H:%M:%S"), compareNextTime.strftime("%Y-%m-%d %H:%M:%S"), code)
            rcmp = con.execute("select wind_code, datetime(time), price, volume from market where time >= datetime(?) "
                               "and time < datetime(?) and wind_code=? ", tcmp)
            rcmp = rcmp.fetchone()
            print rcmp

            if r is None or rcmp is None:
                continue
            timeDelta = (datetime.datetime.strptime(r[1], "%Y-%m-%d %H:%M:%S")
                         - datetime.datetime.strptime(rcmp[1], "%Y-%m-%d %H:%M:%S"))
            seconds = timeDelta.days * 24 * 3600 + timeDelta.seconds
            stockMap[code].volInc = (r[3] - rcmp[3]) * 300.0 / seconds
            stockMap[code].prcInc = r[2] - rcmp[2]
            stockMap[indexName].volInc += stockMap[code].volInc
            stockMap[indexName].prcInc += stockMap[code].prcInc

        # 整理的衡量比较
        if stockMap[indexName].volInc / stockMap[indexName].avg_vol > 1.5 and stockMap[indexName].prcInc > 0:
            sorted(stockMap.iteritems(), key=lambda (k, v): v, cmp=cmpRatio)
            # buy top two stock
            canStockList = filter(lambda x: x.is_have != 0, stockMap.iteritems)
            for stock in canStockList:
                stock.buy()
        exit()

w.stop()


def cmpRatio(x, y):
   sx = float(x.volInc)/x.avg_vol
   sy = float(y.volInc)/y.avg_vol
   return sx < sy


