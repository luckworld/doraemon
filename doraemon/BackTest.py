# coding=utf-8

__author__ = 'a11'

from WindPy import w
from datetime import *
import numpy as np
from StockInfo import *
from init import *


class BackTest:
    def __init__(self, begin_time, end_time, test_name, stocks):
        self.begin_time = begin_time
        self.end_time = end_time
        self.strategy_name = test_name
        self.stocks = stocks
        self.dates = []

    def __prepare_data(self, length=10):
        date = w.tdays(self.begin_time, self.end_time).Data
        self.dates = date[0]

        if len(self.dates) < length:
            logging.info("w.tdays error.....len:%s" % len(self.dates))
            return False

        return True

    def __day_execute(self, one_day):
        logging.info("***************** %s *******************" % one_day)

        dash_line_date = one_day.strftime('%Y%m%d')

        # 获取昨天的前复权收盘价
        candidate_codes_close_price(stocks, dash_line_date, 1)

        # 获取前五天的平均成交量 (不含今日)
        candidate_codes_average_volume(stocks, dash_line_date, 5)

        # logging.info "stocks[0]->", stocks[0]
        # logging.info "stocks[1]->", stocks[1]

        # 获取分钟级别的数据
        start_time = one_day.strftime("%Y-%m-%d") + " 09:00:00"
        end_time = one_day.strftime("%Y-%m-%d") + " 15:01:00"
        for stock in stocks:
            stock.daily_init()
            wsi_data = w.wsi(stock.stock_name, "open, volume", start_time, end_time, PriceAdj="F")
            if wsi_data.ErrorCode != 0:
                w.bktend()
                logging.info("BKT has finished. Error: w.wsi has error")
                return
            # print ("wsi_data->", wsi_data)
            stock.wsi_open_price_data = wsi_data.Data[0]
            stock.wsi_volume_data = wsi_data.Data[1]
            stock.wsi_times = wsi_data.Times

        # stock size = 0 return
        if len(stocks) == 0:
            logging.info("stocks size is zero")
            return

        # 遍历分钟级别的数据进行计算
        price_len = len(stocks[0].wsi_open_price_data)
        logging.info("minute price seq length->%s" % price_len)
        for j in range(price_len):
            for stock in stocks:
                # logging.info j
                if (j - 32) % 15 == 0 and stock.can_buy(j, 1.5):
                    fund_share_cnt = 0
                    for stock in stocks:
                        if stock.is_have == 0:
                            fund_share_cnt += 1
                    stock.buy_stock(j, fund_share_cnt)

                if stock.can_sell(j):
                    stock.sell_stock(j)
        return

    def __minute_execute(self):
        pass

    def execute(self):
        self.__prepare_data()
        logging.debug(self.dates)

        out = w.bktstart(self.strategy_name, self.begin_time, self.end_time, Period="1d", InitialFund=5000000)
        if out.ErrorCode != 0:
            logging.info("Error on w.bktstart! May strategyname is running!")
            logging.info(out)

            return

        # 回测的id
        bktid = out.Data[1]
        strategy_id = out.Data[0]
        logging.info("#" * 20)
        logging.info("stragtegy_id=%s, bktid= %s, begintime=%s, endtime=%s" % (strategy_id, bktid, self.begin_time,
                                                                               self.end_time))

        for one_day in self.dates:
            self.__day_execute(one_day)

        w.bktend()
        logging.info("BTK has finished")
        return 1


w.start()
stocks = [StockInfo("600000.SH"), StockInfo("000006.SZ")]
back_test = BackTest(begin_time="20150410", end_time="20150430", test_name="volumeInfo_python", stocks=stocks)
back_test.execute()

w.stop()
