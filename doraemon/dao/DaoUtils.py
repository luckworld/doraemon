__author__ = 'a11'

import sqlite3

# sqllite3 数据库
cx = sqlite3.connect(":memory:")
cu = cx.cursor()
cu.execute("create table market(id integer primary key autoincrement, wind_code VARCHAR(30), time INTEGER, "
           "volume REAL, price REAL);")
cu.execute("CREATE UNIQUE INDEX idx_code_time ON market(wind_code, time);")
for t in[(0, 'SHSHSHS', '2015-10-22', 100, 100.1), (1, 'SHSHSHS', '2015-10-22', 101, 100.2)]:
    try:
        cx.execute("insert into market values (?,?,?,?,?)", t)
    except Exception, ex:
        print ex

cx.commit()

cu.execute("select * from market")
print cu.fetchall()

cu.execute("update market set wind_code='Boy' where id = 0")
cx.commit()

cu.execute("delete from market where id = 1")
cx.commit()

x = u'鱼'
cu.execute("update market set wind_code=? where id = 0", x)
cu.execute("select * from market")
print cu.fetchall()

exit(0)


