# -*- coding: utf8 -*-

__author__ = 'Administrator'

import MySQLdb


class DBUtil:
    def __init__(self, host='localhost', user='root', db='stocks', port=3306):
        self.host = host
        self.user = user
        self.db = db
        self.port = port
        self.cursor = None
        self.conn = None
        self.getConnection()

    def getSqlResult(self, sql=""):
        count = self.cursor.execute(sql)
        results = self.cursor.fetchall()
        return results

    def getConnection(self):
        if(self.cursor == None):
            self.conn = MySQLdb.connect(host=self.host,user=self.user,db=self.db,port=self.port)
            self.cursor = self.conn.cursor()
            self.cursor.execute('set charset utf8')
        return self.cursor

    def closeConnection(self):
        if self.cursor != None:
            self.cursor.close()
        if self.conn != None:
            self.conn.close()

    def getValStr(self, value=[]):
        if(value == None or len(value) == 0):
            return
        valStr=''
        for val in value:
            valStr = valStr + '%s,'
        valStr = valStr[:-1]
        return valStr

    def getInsertSql(self, tableName = '', fields=[], value=[]):
        valStr = self.getValStr(value)
        fieldStr = self.getInsertFileds(fields)
        sqlStr =  'insert into ' + tableName +' ' +  fieldStr + ' values(' + valStr + ")"
        return sqlStr

    def insertOneLine(self, tableName='', value=[]):
        if(value == None or len(value) == 0):
            return
        sqlStr = self.getInsertSql(tableName,value)
        print sqlStr
        self.cursor.execute(sqlStr,value)
        self.conn.commit()

    def getInsertFileds(self, fields=[]):
        tmpStr =''
        for field in fields:
            tmpStr = tmpStr + field + ','
        tmpStr = tmpStr[:-1]
        filedsStr = '(' + tmpStr + ')'
        return filedsStr

    def insertManyList(self, tableName='', fields=[], values=[]):
        if(values == None or len(values) == 0):
            return
        oneValue = values[0]
        sqlStr = self.getInsertSql(tableName,fields,oneValue)

        print sqlStr
        self.cursor.executemany(sqlStr,values)
        self.conn.commit()


if __name__ == '__main__':
    try:
        dbUtil = DBUtil()
        ##cur = dbUtil.getConnection()
        ##cur.execute('set charset utf8')
        results = dbUtil.getSqlResult(sql='select * from t_stock limit 20')
        print 'there has %s rows record' % len(results)

        ##dbUtil.closeConnection()
        ##results = cur.fetchall();
        for r in results:
            print r[2].decode('utf-8')
        dbUtil.closeConnection()
        '''
        dbUtil.insertManyList(tableName='test',values=[(3,'hello test3'),(4,'hello test4')])
        dbUtil.insertOneLine(tableName="test",value=[5,"fksf"])

        cur.execute('create table test(id int,info varchar(20))')
        value=[1,'hi rollen']
        cur.execute('insert into test values(%s,%s)',value)

        values=[]
        for i in range(20):
            values.append((i,'hi rollen'+str(i)))

        cur.executemany('insert into test values(%s,%s)',values)

        cur.execute('update test set info="I am rollen" where id=3')
        '''
        ##dbUtil.closeConnection()
    except MySQLdb.Error ,e:
        print "Mysql Erro %d:%s" % (e.args[0],e.args[1])