# coding=utf-8

__author__ = 'a11'

from WindPy import w
import math
import logging
import sys
from mylogger import *


class AnalogAccount:
    def __init__(self):
        self.stocks_login_id = 0
        self.stocks_user_account = "w604122601"
        self.stocks_user_pwd = "******"
        self.stocks_login_type = "SHSZ"

        self.capital = None
        self.position = None

        # self.hkd_login_id = 0
        # self.hkd_user_account = "w604122604"
        # self.hkd_user_pwd = "******"

    def login_shsz(self):
        login_info = w.tlogon("0000", "0", self.stocks_user_account, self.stocks_user_pwd, self.stocks_login_type)
        if login_info.ErrorCode != 0:
            print login_info.Fields, "\t", login_info.Data
            return
        login_map = dict(zip(login_info.Fields, login_info.Data))
        print login_info
        self.stocks_login_id = login_map["LogonID"]
        return

    def login_shsz_out(self):
        if self.stocks_login_id > 0:
            w.tlogout(self.stocks_login_id)

    def query_position(self):
        if self.stocks_login_id <= 0:
            print "please login first, login_id:", self.stocks_login_id
            return
        position = w.tquery("Position", LogonId=self.stocks_login_id)
        self.position = position
        
        return position

    def query_capital(self):
        if self.stocks_login_id <= 0:
            print "please login first, login_id:", self.stocks_login_id
            return
        capital = w.tquery("Capital", LogonId=self.stocks_login_id)
        self.capital = capital
        
        return capital

# w.start()
# a = AnalogAccount()
# a.login_shsz()
# a.query_position()
# a.query_capital()
# a.login_shsz_out()
# w.stop()
