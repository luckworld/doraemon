# coding=utf-8

__author__ = 'a11'

from WindPy import w
import math
import logging
import sys


class StockInfo:
    def __init__(self, stock_name=""):
        self.stock_name = stock_name
        self.is_have = 0
        self.buy_datetime = None
        self.hands = 0
        self.buy_price = None
        self.up_price = None
        self.last_day_close_price = None
        self.average_volume = None
        self.can_sell_today = 1  # 是否可以卖出
        self.is_up_today = 0  # 是否涨停
        self.wsi_open_price_data = None  # 分钟级别的成交价的数据
        self.wsi_volume_data = None  # 分钟级别的成交量的数据
        self.wsi_times = None  # wsi_times
        self.codes = None

    def __str__(self):
        print self.stock_name, self.is_have, self.buy_datetime, self.hands, self.buy_price, self.up_price, \
            self.last_day_close_price, self.average_volume, self.can_sell_today, self.is_up_today
        return ""

    def daily_init(self):
        self.is_up_today = 0
        self.can_sell_today = 1

    # 判断股票是否可以买入
    def can_buy(self, minute_count=0, gamma=1.5):
        tag = False
        if self.is_have == 1 or self.is_have is True:
            return tag
        elif self.average_volume is None or self.average_volume == 0:
            print "avg_vol exception->", self.average_volume
            return tag

        # 计算15分钟的成交量
        sum15vol = sum(self.wsi_volume_data[max(minute_count - 15, 0): minute_count])
        if sum15vol * 18.0 / self.average_volume >= gamma:
            tag = True

        return tag

    def buy_stock(self, minute_count=0, fund_share_cnt=1):

        # print "-->", "#" * 10, "buy_stock", "#" * 10
        print self.wsi_times[minute_count].strftime("%Y-%m-%d %H:%M:%S")

        out = w.bktquery("Capital", self.wsi_times[minute_count])
        if out.ErrorCode != 0:
            print("Error on w.bktquery--Capital! out: %s", out)
            return

        # print out
        out_map = dict(zip(out.Fields, out.Data))
        total_fund = out_map['AvailableFund']  # out.Data.AvailableFund
        if total_fund is None or total_fund <= 0:
            print out
            return
        print "available fund->", total_fund

        # hands
        volume = math.floor(total_fund / fund_share_cnt / self.wsi_open_price_data[minute_count] * 0.9 / 100) * 100
        print "could buy hands->", volume
        if volume is None:
            print "volume is invalid, out:%s, volume:%s, total_fund:%s, fund_share_cnt:%s, price:%s" \
                  % (out, volume, total_fund, fund_share_cnt, self.wsi_open_price_data[j])
            return
        elif volume < 100:
            print "volume < 100, can not buy, volume:%s" % volume
            return

        order_out = w.bktorder(self.wsi_times[minute_count], self.stock_name, u"buy", volume)
        print "buy order res->"
        if order_out.ErrorCode != 0:
            print "Error on w.bktorder buy! order_out:%s, volume: %s" % (order_out, volume)
            return

        self.is_have = 1
        self.hands = volume
        self.buy_price = self.wsi_open_price_data[minute_count]

        debug = True
        if debug is True:
            print self.wsi_times[minute_count - 1], "\t", self.wsi_open_price_data[minute_count - 1], " fund:", \
                total_fund
            out = w.bktquery("Capital", self.wsi_times[minute_count])
            out_map = {'AvailableFund': -1}
            if out.ErrorCode == 0:
                out_map = dict(zip(out.Fields, out.Data))
            print "Buy...", self.wsi_times[minute_count], "\t", self.wsi_open_price_data[minute_count], "\t", volume, " ", \
                self.stock_name, " ", out_map['AvailableFund']
            print self.wsi_times[minute_count + 1], "\t", self.wsi_open_price_data[minute_count + 1]

        return

    def can_sell(self, minute_count=0):
        # print "-->", "*" * 15, "can sell judge ", minute_count, "*" * 15

        tag = False
        if self.is_have == 0 or self.is_have is False:  # no hands, can not sell
            # print "--> is_have:", self.is_have
            return tag
        elif self.hands is None or self.hands <= 0:
            # print "--> hands:", self.hands
            return tag
        elif self.can_sell_today == 0:  # 是否已经卖出过 0 已经卖出过
            # print "--> can_sell_today:", self.can_sell_today
            return tag
        elif minute_count == 231:  #
            if self.wsi_open_price_data[minute_count] / self.last_day_close_price >= 1.095:  # 涨停
                self.up_price = self.wsi_open_price_data[minute_count]
                self.is_up_today = 1
                return False
            else:
                return True
        elif self.buy_price is None or self.wsi_open_price_data[minute_count] >= self.buy_price:
            print "--> current price > buy price:", self.wsi_open_price_data[minute_count], " ", self.buy_price
            return False
        # elif self.is_up_today == 1:
        #     return False

        return True

    def sell_stock(self, minute_count=0):
        self.before_sell(minute_count)
        if minute_count == 231:
            # 卖出股票
            out = w.bktorder(self.wsi_times[minute_count], self.stock_name, u"sell", self.hands)
            if out.ErrorCode != 0:
                print "sell stock error! out:%s" % out
                w.bktend()
                exit()
                return

            # 标记数据
            self.after_sell(minute_count)
            return

        #  涨停 持有股票的  若T＋1涨停，T＋2跌破T＋1涨停价卖出
        if self.is_up_today == 0 and self.up_price is not None \
                and self.wsi_open_price_data[minute_count] < self.up_price:
            out = w.bktorder(self.wsi_times[minute_count], self.stock_name, u"sell", self.hands)
            if out.ErrorCode != 0:
                print "sell stock error, out:%s" % out
                w.bktend()
                exit()
                return

            # 标记数据
            self.after_sell(minute_count)
            return

        # 普通
        if self.buy_price is None or self.wsi_open_price_data[minute_count] >= self.buy_price:
            return
        if self.is_up_today == 1:
            return

        if self.wsi_open_price_data[minute_count] / self.buy_price < 0.98:
            out = w.bktorder(self.wsi_open_price_data[minute_count], self.stock_name, u"sell", self.hands)
            if out.ErrorCode != 0:
                print "sell stock error, out:%s" % out
                return

            # 标记数据
            self.after_sell(minute_count)
            return

        return

    def before_sell(self, minute_count=1):
        out = w.bktquery("Capital", self.wsi_times[minute_count])
        out_map = {'AvailableFund': -1}
        if out.ErrorCode == 0:
            out_map = dict(zip(out.Fields, out.Data))
        logging.info("Before Sell...time:%s, price:%s, hands:%s, code:%s, avaFund:%s" % (self.wsi_times[minute_count],
                     self.wsi_open_price_data[minute_count], self.hands, self.stock_name, out_map['AvailableFund']))

    # 标记数据
    def after_sell(self, minute_count=1):
        logging.info("After Selling....")
        print "*" * 22
        print self.wsi_times[minute_count - 1], "\t", self.wsi_open_price_data[minute_count - 1]
        out = w.bktquery("Capital", self.wsi_times[minute_count])
        out_map = {'AvailableFund': -1}
        if out.ErrorCode == 0:
            out_map = dict(zip(out.Fields, out.Data))
        print "Sell...", self.wsi_times[minute_count], "\t", self.wsi_open_price_data[minute_count], "\t", self.hands, \
            " ", self.stock_name, " ", out_map['AvailableFund']
        print self.wsi_times[minute_count + 1], "\t", self.wsi_open_price_data[minute_count + 1]

        self.is_have = 0
        self.buy_price = None
        self.up_price = None
        self.can_sell_today = 0
        return
