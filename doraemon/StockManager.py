# coding=utf-8

from StockIndex import *
from StockInfo import *
import time
from WindPy import w


class StockManager:

    def __init__(self):
        self.stock_index_list = []
        self.stock_index_map = {}
        self.stock_code = "884001.WI"
        self.sector_name = u"WIND概念板块指数"

    @staticmethod
    def __get_children_codes(stock_code):
        date_str = time.strftime("%Y%m%d", time.localtime(time.time()))
        codes = w.wset("SectorConstituent", date=date_str, windcode=stock_code)
        if codes.ErrorCode != 0:
            print codes
            return

        return codes

    # w_wset_data<-w.wset('SectorConstituent','date=20150730;sector=WIND概念板块指数')
    @staticmethod
    def __get_sector_codes(sector_name):
        date_str = time.strftime("%Y%m%d", time.localtime(time.time()))
        codes = w.wset('SectorConstituent', date=date_str, sector=sector_name)
        if codes.ErrorCode != 0:
            print codes
            return

        return codes

    def get_stock_index_info(self):
        codes = self.__get_sector_codes(self.sector_name)
        if codes.ErrorCode != 0:
            print codes
            return
        print codes
        map_info = dict(zip(codes.Fields, codes.Data))
        children_wind_codes = map_info['wind_code']
        print "debug:", children_wind_codes

        for children in children_wind_codes:
            children_info = self.__get_children_codes(children)
            self.stock_index_list.append(children_info)
            map_info = dict(zip(children_info.Fields, children_info.Data))
            children_codes = map_info['wind_code']
            print "debug:", children_codes
            stock_index = StockIndex()
            for single_child_code in children_codes:
                # single_child_info = self.__get_children_codes(single_child_code)
                stock_info_tmp = StockInfo(single_child_code)
                # stock_info_tmp.codes = single_child_info
                stock_index.children.append(stock_info_tmp)

            self.stock_index_map[children] = stock_index
        return
