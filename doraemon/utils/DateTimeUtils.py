# -*- coding: utf-8 -*-

# @file DateTimeUtils
# @brief ''
# @author 'a11'
# @version '0.1'
# @date 16/1/11

import time


class DateTimeUtils(object):
    SHORT_PATTERN = "%Y%m%d"
    LONG_PATTERN = "%Y%m%d%H%M%s"
    SHORT_DISPLAY_PATTERN = "%Y-%m-%d"
    LONG_DISPLAY_PATTERN = "%Y-%m-%d %H:%M:%S"
    FIRST_DATE_PATTERN = "%Y-%m-01"
    MOVIEB_DATE_PATTERN = "%Y年%m月%d日 %H:%M"

    @staticmethod
    def format_time(arg_time=None, time_format=None):
        localtime = arg_time if arg_time is not None else time.localtime()

        if time_format is None:
            time_string = time.strftime(DateTimeUtils.LONG_PATTERN, localtime)
        else:
            time_string = time.strftime(time_format, localtime)
        return time_string

    @staticmethod
    def format_time_long_pattern(arg_time=None):
        return DateTimeUtils.format_time(arg_time, DateTimeUtils.LONG_PATTERN)

    @staticmethod
    def display_date():
        return DateTimeUtils.format_time(time.localtime(), DateTimeUtils.SHORT_DISPLAY_PATTERN)

    @staticmethod
    def display_date_time():
        return DateTimeUtils.format_time(time.localtime(), DateTimeUtils.LONG_DISPLAY_PATTERN)

    @staticmethod
    def get_now_string():
        return DateTimeUtils.format_time_long_pattern(time.localtime())

    @staticmethod
    def get_now_string(pattern=None):
        return DateTimeUtils.format_time(time.localtime(), pattern)

    @staticmethod
    def parse(date_str, pattern):
        from datetime import datetime
        return datetime.strptime(date_str, pattern)

    @staticmethod
    def parse_by_date_util(date_str):
        """
        handle most date format, can guess the correct format most of the time.
        :param date_str:
        :return:
        """
        from dateutil import parser
        return parser.parse(date_str)

    @staticmethod
    def parse_with_display_pattern(date_str=None):
        try:
            return DateTimeUtils.parse(date_str, DateTimeUtils.LONG_DISPLAY_PATTERN)
        except:
            return DateTimeUtils.parse(date_str, DateTimeUtils.SHORT_DISPLAY_PATTERN)

    @staticmethod
    def time2int(t):
        """ datetime转化为整数。

            :param datetime t: 时间。
            :return: 整数。
            :rtype: int
         """
        epoch = int(time.mktime(t.timetuple())*1000)
        return epoch

if __name__ == '__main__':
    print DateTimeUtils.get_now_string(DateTimeUtils.LONG_DISPLAY_PATTERN)
    print DateTimeUtils.parse("2015-09-01", DateTimeUtils.SHORT_DISPLAY_PATTERN)
    print DateTimeUtils.display_date()
    print DateTimeUtils.display_date_time()
    print DateTimeUtils.get_now_string(DateTimeUtils.FIRST_DATE_PATTERN)
    print DateTimeUtils.get_now_string(DateTimeUtils.MOVIEB_DATE_PATTERN)
