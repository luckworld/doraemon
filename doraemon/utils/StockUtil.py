# coding=utf-8

from WindPy import w
from datetime import *
import numpy as np

# class - object - method dynamic functional - property static data
# get average volume for stock in sometime
# end_date : 2015-10-01


def stocks_average_volume(stocks, end_date, days_interval_len=5):
    # code_list = [x.stock_code for x in stocks]
    code_list = stocks
    res = {}

    if not isinstance(code_list, list):
        print("stock codes variable should be a list, actual:%s" % type(code_list))
        return res

    if days_interval_len <= 0:
        print "trade days must be positive integer"
        return res

    # actual_end_date
#    last_date_info = w.tdaysoffset(-1, end_date)
#    if last_date_info.ErrorCode != 0:
#        print('w.tdaysoffset get actual end date error: %s , %s last_date_info<-w.tdaysoffset(-1, end_date);]'
#              % end_date, last_date_info)
#        return res
#    actual_end_date = last_date_info.Data[0][0].strftime("%Y-%m-%d")

    # actual_start_date
#    actual_start_date_info = w.tdaysoffset(-days_interval_len, end_date)
#    if actual_start_date_info.ErrorCode != 0:
#        print("w.tdaysoffset error, error get %s" % actual_start_date_info)
#        return res
#    actual_start_date = actual_start_date_info.Data[0][0].strftime("%Y-%m-%d")
    start_date = get_offset_trade_date(end_date, -days_interval_len)

    # obtain data
    codes_vol_data = w.wsd(code_list, "volume", start_date, end_date);
    if codes_vol_data.ErrorCode != 0:
        print ("ERROR: [w.wsd error code, codes_vol_data<-w.wsd(codesStr, \"volume\", actual_start_date, lenddate);]")
        raise ValueError("Error: get volumes")
        return res

    print codes_vol_data

    return codes_vol_data

    # append res
#    avg_vol_vec = np.mean(codes_vol_data.Data, axis=1)  # 1 columns, 0 rows

    # res = dict()
    # res['Codes'] = codes_vol_data.Codes
    # res['Times'] = codes_vol_data.Times
    # res['Data'] = codes_vol_data.Data
    # res['Fields'] = codes_vol_data.Fields
    # res['ErrorCode'] = codes_vol_data.ErrorCode
    # res['avg_vol'] = avg_vol_vec

#    codes_map = dict(zip(codes_vol_data.Codes, avg_vol_vec))
#    for stock in stocks:
#        stock.average_volume = codes_map.get(stock.stock_name)
#        print stock.stock_name, "->avg vol->", stock.average_volume
#
#    return


def stocks_close_price(stocks, date, days_interval_len = 1):
    code_list = [x.stock_name for x in stocks]
    res = None

    if not isinstance(code_list, list):
        print("stock codes variable should be a list, actual:%s" % type(code_list))
        return res

    if days_interval_len <= 0:
        print "trade days must be positive integer"
        return res

    actual_date_info = w.tdaysoffset(-days_interval_len, date, "Days=Alldays")
    if actual_date_info.ErrorCode != 0:
        print("w,tdaysoffset error : can not find n days before date: %s, %s" % date, actual_date_info)
        return res
    actual_date = actual_date_info.Data[0][0].strftime('%Y%m%d')

    actual_trade_date_info = w.tdaysoffset(0, actual_date)
    if actual_trade_date_info.ErrorCode != 0:
        print ("w.tdaysoffset get trading date error!")
        return res
    actual_trade_date = actual_trade_date_info.Data[0][0].strftime("%Y%m%d")

    # 获取收盘价 前复权收盘价
    close_prices = w.wsd(code_list, "CLOSE", beginTime=actual_trade_date, endTime=actual_trade_date, PriceAdj="F")
    if close_prices.ErrorCode != 0:
        print ("Error: [w.wsd get close price error]")
        return res

    # print(close_prices)
    codes_map = dict(zip(close_prices.Codes, close_prices.Data[0]))

    for stock in stocks:
        stock.last_day_close_price = codes_map.get(stock.stock_name)
        print stock.stock_name, "->close price->", stock.last_day_close_price

    return

# w.start()
# codes = [StockInfo("600000.SH"), StockInfo("000006.SZ")]
# candidate_codes_close_price(codes, "2015-08-20", 5)
# print "$" * 20
# print codes
# print "#" * 20
# w.stop()


def get_offset_trade_date(current_date, offset):
    last_date_info = w.tdaysoffset(offset, current_date)

    if last_date_info.ErrorCode != 0:
        print('w.tdaysoffset get actual end date error: %s , %s last_date_info<-w.tdaysoffset(-1, end_date);]'
              % today, last_date_info)
        raise ValueError("tdaysoffset func exec error")
    end_date = last_date_info.Data[0][0].strftime("%Y-%m-%d")

    return end_date


