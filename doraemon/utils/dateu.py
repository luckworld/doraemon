# -*- coding:utf-8 -*-

import datetime
import time
import pandas as pd
from datetime import timedelta


def year_qua(date):
    mon = date[5:7]
    mon = int(mon)
    return[date[0:4], _quar(mon)]


def _quar(mon):
    if mon in [1, 2, 3]:
        return '1'
    elif mon in [4, 5, 6]:
        return '2'
    elif mon in [7, 8, 9]:
        return '3'
    elif mon in [10, 11, 12]:
        return '4'
    else:
        return None


def today():
    day = datetime.datetime.today().date()
    return str(day)


def get_year():
    year = datetime.datetime.today().year
    return year


def get_month():
    month = datetime.datetime.today().month
    return month


def get_hour():
    return datetime.datetime.today().hour


def today_last_year():
    lasty = datetime.datetime.today().date() + datetime.timedelta(-365)
    return str(lasty)


def day_last_week(days=-7):
    lasty = datetime.datetime.today().date() + datetime.timedelta(days)
    return str(lasty)


def get_now():
    return time.strftime('%Y-%m-%d %H:%M:%S')


def diff_day(start=None, end=None):
    d1 = datetime.datetime.strptime(end, '%Y-%m-%d')
    d2 = datetime.datetime.strptime(start, '%Y-%m-%d')
    delta = d1 - d2
    return delta.days


def get_quarts(start, end):
    idx = pd.period_range('Q'.join(year_qua(start)), 'Q'.join(year_qua(end)), freq='Q-JAN')
    return [str(d).split('Q') for d in idx][::-1]


holiday = ['2015-01-01', '2015-01-02', '2015-02-18', '2015-02-19', '2015-02-20', '2015-02-23', '2015-02-24',
           '2015-04-06', '2015-05-01', '2015-06-22', '2015-09-03', '2015-09-04', '2015-10-01', '2015-10-02',
           '2015-10-05', '2015-10-06', '2015-10-07',
           '2016-01-01', '2016-02-07', '2016-02-08', '2016-02-09', '2016-02-10', '2016-02-11', '2016-02-12',
           '2016-02-13', '2016-04-04', '2016-05-01', '2016-05-02', '2016-06-09', '2016-06-10', '2016-06-11',
           '2016-09-15', '2016-09-16', '2016-09-17', '2016-10-01', '2016-10-02', '2016-10-03', '2016-10-04',
           '2016-10-05', '2016-10-06', '2016-10-07']


def is_holiday(date):
    if isinstance(date, str):
        date = datetime.datetime.strptime(date, '%Y-%m-%d')
    today = int(date.strftime("%w"))
    date_str = date.strftime("%Y-%m-%d")
    if today > 0 and today < 6 and date_str not in holiday:
        return False
    else:
        return True


def last_tddate():
    today = datetime.datetime.today().date()
    today = int(today.strftime("%w"))
    if today == 0:
        return day_last_week(-2)
    else:
        return day_last_week(-1)


def time2datetime(mytime):
    """
    convert time to date time
    Notice: time到datetime的转换是忽略小数点后面的数值
    @see http://blog.csdn.net/hong201/article/details/3193121
    :param mytime: time
    :return: date time
    """
    t = mytime.time()
    return datetime.datetime.fromtimestamp(t)


def datetime2time(mydatetime):
    """
    convert datetime to time
    http://stackoverflow.com/questions/8022161/python-converting-from-datetime-datetime-to-time-time
    :param mydatetime: date time
    :return: time
    """
    if isinstance(mydatetime, datetime.datetime):
        return time.mktime(mydatetime.timetuple()) + mydatetime.microsecond / 1E6
    elif isinstance(mydatetime, datetime.date):
        return time.mktime(mydatetime.timetuple())
    else:
        return time.mktime(mydatetime.timetuple())


def datetime2timestruct(mydatetime):
    """
    http://stackoverflow.com/questions/5657308/how-do-i-convert-a-datetime-date-object-into-a-time-struct-time-object
    :param mydatetime: date time
    :return: time structure
    """
    return mydatetime.timetuple()


def timestruct2datetime(mytime):
    """
    http://stackoverflow.com/questions/1697815/how-do-you-convert-a-python-time-struct-time-object-into-a-datetime-object
    :param mytime: time structure
    :return: date time
    """
    t = time.mktime(mytime)
    return datetime.datetime.fromtimestamp(t)


def time2timestruct(mytime):
    return datetime2time(timestruct2datetime(mytime))


def timestruct2time(mytime):
    return datetime2timestruct(time2datetime(mytime))


def daterange(start_date, end_date):
    """
    返回两个date之间的日期迭代器
    :param start_date: 开始期间
    :param end_date: 结束日期
    :return: 两个日期之间的迭代器
    """
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)


def trade_day_offset(start_date, offset = 0):
    zero_point = start_date

    while is_holiday(zero_point) is False:
        zero_point = zero_point + timedelta(-1)

    if offset == 0:
        return zero_point
    else:
        cnt = abs(offset)
        if offset > 0:
            step = 1
        else:
            step = -1
        while cnt > 0:
            zero_point = zero_point + timedelta(step)
            if is_holiday(zero_point) is False:
                cnt -= 1

    return zero_point


if __name__ == '__main__':
    from datetime import timedelta, date
    from doraemon.utils.DateTimeUtils import *

    def daterange(start_date, end_date):
        for n in range(int ((end_date - start_date).days)):
            yield start_date + timedelta(n)

    start_date = date(2016, 1, 1)
    end_date = date(2016, 12, 31)

    print trade_day_offset(start_date, -4)
    print start_date + timedelta(-1)

    print datetime2timestruct(start_date)

    for single_date in daterange(start_date, end_date):
        if is_holiday(DateTimeUtils.format_time(datetime2timestruct(single_date), DateTimeUtils.SHORT_DISPLAY_PATTERN)) is False:
            print single_date.strftime("%Y-%m-%d"), int(single_date.strftime("%w"))
