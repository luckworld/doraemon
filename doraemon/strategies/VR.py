# -*- coding: utf-8 -*-

# @file VR
# @brief ''
# @author 'a11'
# @version '0.1'
# @date 16/1/14

"""
ratio = sum(last_vol) / daily_vol_n_days

"""

import MySQLdb

from doraemon.utils.dateu import *
from doraemon.utils.DateTimeUtils import DateTimeUtils


def avg_days_vol(code="", s_day_str=None, days_offset=0):
    if s_day_str is None:
        s_day_str = DateTimeUtils.display_date()
    s_date = DateTimeUtils.parse(s_day_str, DateTimeUtils.SHORT_DISPLAY_PATTERN)
    e_date = trade_day_offset(s_date, days_offset)

    print s_date, e_date
    cnt, t_market_days = get_t_market_trade_date(code, s_date, e_date)
    sum_vol = 0
    for r in t_market_days:
        sum_vol += r[5]

    return sum_vol / cnt


def all_avg_vol(s_day_str=None, days_offset=0):
    if s_day_str is None:
        s_day_str = DateTimeUtils.display_date()
    s_date = DateTimeUtils.parse(s_day_str, DateTimeUtils.SHORT_DISPLAY_PATTERN)
    e_date = trade_day_offset(s_date, days_offset)

    print s_date, e_date
    cnt, t_market_days = get_t_market_trade_date(None, s_date, e_date)

    # TODO 形成code->平均交易量的字典

    return []


def get_t_market_trade_date(code=None, s_date="2016-01-01", e_date="2015-01-02"):
    t_market_days = []

    conn = MySQLdb.connect(host='10.211.55.2', user='root', db='stocks', port=3306)
    cursor = conn.cursor()
    cursor.execute("set charset utf8")

    if code is not None:
        print 'select * from t_market_day where trade_date >= "%s" and trade_date<="%s" and stock_code="%s"' % (e_date, s_date, code)
        sql = 'select * from t_market_day where trade_date >= "%s" and trade_date<="%s" and stock_code="%s"' % (e_date, s_date, code)
    else:
        print 'select * from t_market_day where trade_date >= "%s" and trade_date<="%s" ' % (e_date, s_date, code)
        sql = 'select * from t_market_day where trade_date >= "%s" and trade_date<="%s" ' % (e_date, s_date, code)

    cnt = cursor.execute(sql)
    result = cursor.fetchall()
    for r in result:
        t_market_days.append(r)
        print r
    cursor.close()

    print 'there has %s rows record' % cnt
    conn.close()

    return cnt, t_market_days

if __name__ == '__main__':
    print avg_days_vol("000001.SZ", "2016-01-01", -4)
