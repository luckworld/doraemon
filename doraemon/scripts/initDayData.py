# -*- coding: utf8 -*-

"""
    用途：每天定时将最近一天的全部市场的股票的收盘价、成交量等数据获取下来，并存入到数据库中
    :ivar -s start    同步数据的开始日期，default: 今天，格式 2016-01-11
    :ivar -d dayCount 多少天范围的数据,  default: 1
    :ivar -h host     主机地址，        default: 127.0.0.1 eg: 10.211.55.2 mysql数据库的地址
    :ivar -p port     端口，           default:3306
    :ivar -u user     用户名，         default:root
    :ivar -P password 密码，          default:""
"""

import sys
import getopt
import datetime
import MySQLdb

from doraemon.dao.DBConnection import *
from doraemon.utils.DateTimeUtils import *
from WindPy import w


def usage():
    print("-s start    同步数据的开始日期，default: 今天，格式 2016-01-11")
    print("-d dayCount 多少天范围的数据,  default: 1")
    print("-H host     主机地址，        default: 127.0.0.1 eg: 10.211.55.2 mysql数据库的地址")
    print("-p port     端口，           default:3306")
    print("-u user     用户名，         default:root")
    print("-P password 密码，          default:""")
    print("-h help")

opts, args = getopt.getopt(sys.argv[1:], "hs:d:H:p:u:P:")
if not opts:
    usage()
    exit(0)

start = ""
dayCount = 1
dbHost = "127.0.0.1"
port = 3306
user = "root"
pswd = ""
for op, value in opts:
    if op == "-h":
        usage()
        sys.exit(0)
    elif op == "-s":
        start = value
    elif op == "-d":
        dayCount = value
    elif op == "-H":
        dbHost = value
    elif op == "p":
        port = value
    elif op == "-u":
        user = value
    elif op == "-P":
        pswd = value


def getFloatDefaultVal(val=None):
    """
    处理wind提供的nan数据
    :param val: wind平台提供的交易信息数据
    :return: 无法处理的数据，转换为－1.0存储
    """
    res = -1.0
    try:
        if val is None or val == 'nan' or val == '':
            return -1.0
        res = float(val)
    except:
        return -1.0
    finally:
        if res is None:
            res = -1.0
        if str(res).isalpha():
            res = -1.0
        return res


def convert_price_to_fen(val=None):
    """
    将价格转化为分
    :param val:
    :return:
    """
    if val > 0:
        return val * 100
    else:
        return val


def get_stocks():
    """
    从股票表中获取所有的股票数据
    :return: count, 股票数据
    """
    global conn
    stocks = []

    cursor = conn.cursor()
    cursor.execute("set charset utf8")
    cnt = cursor.execute('select * from t_stock')
    print 'there has %s rows record' % cnt

    result = cursor.fetchall()
    for r in result:
        stocks.append(r)
        print r
    cursor.close()

    return cnt, stocks


def get_wsd_date(stock_code=None, day_count=1):
    start_date = DateTimeUtils.display_date()
    ed_str = "ED-%sD" % (day_count)

    retry = 0
    while retry < 3:
        wsd_data = w.wsd(stock_code, "open,close,volume", ed_str, start_date, "Fill=Previous;PriceAdj=F")
        retry += 1
        if wsd_data.ErrorCode == 0:
            break
        if retry < 3:
            time.sleep(2 ** retry)
            # datetime.time.sleep(2**retry)

    return wsd_data

# conn = MySQLdb.connect(host='10.211.55.2', user='root', db='stocks', port=3306)
conn = MySQLdb.connect(host=dbHost, user=user, db='stocks', port=port)
count, stock_list = get_stocks()
conn.close()

dbUtil = DBUtil(host=dbHost)
count = 0

w.start()
for row in stock_list:
    # print row
    # retry = 0
    # while retry < 3:
    #     w_wsd_data = w.wsd(row[1], "open,close,volume", "ED-20D", "2016-01-04", "Fill=Previous;PriceAdj=F")
    #     retry += 1
    #     if w_wsd_data.ErrorCode == 0:
    #         break
    #     datetime.time.sleep(1)
    # print w_wsd_data

    w_wsd_data = get_wsd_date(row[1], day_count=dayCount)
    code = w_wsd_data.Codes[0]
    print code

    times = w_wsd_data.Times
    print times

    openPrice = w_wsd_data.Data[0]
    closePrice = w_wsd_data.Data[1]
    volume = w_wsd_data.Data[2]
    values = []
    lenVal = len(times)

    cur = conn.cursor()
    dbUtil.getConnection()

    for i in range(lenVal):
        value = []
        # value.append(i)
        value.append(code)
        value.append(times[i])

        value.append(convert_price_to_fen(getFloatDefaultVal(openPrice[i])))
        value.append(convert_price_to_fen(getFloatDefaultVal(closePrice[i])))
        value.append(getFloatDefaultVal(volume[i]))
        #value.append('')
        #value.append('20160103')
        #value.append('20160103')
        ##  print value
        ## dbUtil.insertOneLine(tableName='t_market_day',value=value)

        values.append(value)
        count += 1
        if count % 100 == 0:
            dbUtil.insertManyList(tableName='t_market_day',
                                  fields=['stock_code', 'trade_date', 'open_price', 'close_price', 'volume'],
                                  values=values)
            print count
            values = []
        ## print value
    dbUtil.insertManyList(tableName='t_market_day',
                          fields=['stock_code', 'trade_date', 'open_price', 'close_price', 'volume'],
                          values=values)
w.stop()
dbUtil.closeConnection()
# data=w.wsd("600000.SH","close,amt","2013-04-30", datetime.today()-timedelta(1))


if __name__ == '__main__':
    print "Hello world"
    start_date = DateTimeUtils.display_date()

