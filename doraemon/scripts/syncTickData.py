# -*- coding: utf8 -*-

"""
    用途：每天定时将最近一天的全部市场的股票的收盘价、成交量等数据获取下来，并存入到数据库中
    :ivar -h host     主机地址，        default: 127.0.0.1 eg: 10.211.55.2 mysql数据库的地址
    :ivar -p port     端口，           default:3306
    :ivar -u user     用户名，         default:root
    :ivar -P password 密码，          default:""
"""

import sys
import getopt
import datetime
import MySQLdb

from doraemon.dao.DBConnection import *
from doraemon.utils.DateTimeUtils import *
from WindPy import w


def usage():
    print("-H host     主机地址，        default: 127.0.0.1 eg: 10.211.55.2 mysql数据库的地址")
    print("-p port     端口，           default:3306")
    print("-u user     用户名，         default:root")
    print("-P password 密码，          default:""")
    print("-h help")

opts, args = getopt.getopt(sys.argv[1:], "hH:p:u:P:")
if not opts:
    usage()
    exit(0)

start = ""
dayCount = 1
dbHost = "127.0.0.1"
port = 3306
user = "root"
pswd = ""
for op, value in opts:
    if op == "-h":
        usage()
        sys.exit(0)
    elif op == "-s":
        start = value
    elif op == "-d":
        dayCount = value
    elif op == "-H":
        dbHost = value
    elif op == "p":
        port = value
    elif op == "-u":
        user = value
    elif op == "-P":
        pswd = value


def getFloatDefaultVal(val=None):
    """
    处理wind提供的nan数据
    :param val: wind平台提供的交易信息数据
    :return: 无法处理的数据，转换为－1.0存储
    """
    res = -1.0
    try:
        if val is None or val == 'nan' or val == '':
            return -1.0
        res = float(val)
    except:
        return -1.0
    finally:
        if res is None:
            res = -1.0
        if str(res).isalpha():
            res = -1.0
        return res


def convert_price_to_fen(val=None):
    """
    将价格转化为分
    :param val:
    :return:
    """
    if val > 0:
        return val * 100
    else:
        return val


def get_stocks():
    """
    从股票表中获取所有的股票数据
    :return: count, 股票数据
    """
    global conn
    stocks = []

    cursor = conn.cursor()
    cursor.execute("set charset utf8")
    cnt = cursor.execute('select * from t_stock')
    print 'there has %s rows record' % cnt

    result = cursor.fetchall()
    for r in result:
        stocks.append(r)
        print r
    cursor.close()

    return cnt, stocks


def get_wsq_date(stock_codes=None, day_count=1):
    stock_codes_str = ','.join(stock_codes)

    retry = 0
    while retry < 3:
        wsq_data = w.wsq(stock_codes_str, "rt_bid1,rt_ask1,rt_vol")
        retry += 1
        if wsq_data.ErrorCode == 0:
            break
        if retry < 3:
            time.sleep(2 ** retry)

    return wsq_data

# conn = MySQLdb.connect(host='10.211.55.2', user='root', db='stocks', port=3306)
conn = MySQLdb.connect(host=dbHost, user=user, db='stocks', port=port)
count, stock_list = get_stocks()
conn.close()

dbUtil = DBUtil(host=dbHost)
count = 0

w.start()
cnt = 0
# for row in stock_list:
while cnt < len(stock_list):

    tmp_stocks = []
    bunch_step = min(200, len(stock_list) - cnt)
    for i in range(0, bunch_step):
        tmp_stocks.append(stock_list[cnt+i][1])

    w_wsq_data = get_wsq_date(tmp_stocks, day_count=dayCount)
    errNo = w_wsq_data.ErrorCode
    if errNo != 0:
        print w_wsq_data
        time.sleep(2)  # sleep two seconds
        continue
    else:
        cnt += bunch_step

    times = w_wsq_data.Times
    print times
    print w_wsq_data

    cur = conn.cursor()
    dbUtil.getConnection()
    values = []
    for j in range(0, len(w_wsq_data.Codes)):
        code = w_wsq_data.Codes[j]
        print code

        bid1Price = w_wsq_data.Data[0][j]
        ask1Price = w_wsq_data.Data[1][j]
        volume = w_wsq_data.Data[2][j]

        value = []
        # value.append(i)
        value.append(code)
        value.append(times[0])

        value.append(convert_price_to_fen(getFloatDefaultVal(bid1Price)))
        value.append(convert_price_to_fen(getFloatDefaultVal(ask1Price)))
        value.append(getFloatDefaultVal(volume))

        values.append(value)
        count += 1
        if count % 100 == 0:
            dbUtil.insertManyList(tableName='t_market_tick',
                                  fields=['stock_code', 'wsq_return_time', 'bid1_price', 'ask1_price', 'volume'],
                                  values=values)
            print count
            values = []
        ## print value

    dbUtil.insertManyList(tableName='t_market_tick',
                          fields=['stock_code', 'wsq_return_time', 'bid1_price', 'ask1_price', 'volume'],
                          values=values)

w.stop()
dbUtil.closeConnection()
# data=w.wsd("600000.SH","close,amt","2013-04-30", datetime.today()-timedelta(1))


